<?php

namespace app\filters;

class GalleryFilter extends \luya\admin\base\Filter
{
    public static function identifier()
    {
        return 'gallery-filter';
    }

    public function name()
    {
        return 'Gallery Image';
    }

    public function chain()
    {
        return [
            [self::EFFECT_THUMBNAIL, [
                'width' => 495,
                'height' => null,
                'saveOptions' => ['quality' => 80]
            ]],
        ];
    }
}