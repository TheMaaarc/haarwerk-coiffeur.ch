<?php

namespace app\filters;

class LightboxFilter extends \luya\admin\base\Filter
{
    public static function identifier()
    {
        return 'lightbox-filter';
    }

    public function name()
    {
        return 'Lightbox Image';
    }

    public function chain()
    {
        return [
            [self::EFFECT_THUMBNAIL, [
                'width' => 1920,
                'height' => null,
                'saveOptions' => ['quality' => 60]
            ]],
        ];
    }
}