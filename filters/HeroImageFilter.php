<?php

namespace app\filters;

class HeroImageFilter extends \luya\admin\base\Filter
{
    public static function identifier()
    {
        return 'hero-image-filter';
    }

    public function name()
    {
        return 'Hero Image';
    }

    public function chain()
    {
        return [
            [self::EFFECT_THUMBNAIL, [
                'width' => 1920,
                'height' => null,
                'saveOptions' => ['quality' => 50]
            ]],
        ];
    }
}