var hero = function () {
    var $hero = $('.hero__items').flickity({
        cellAlign: 'left',
        contain: true,
        prevNextButtons: false
    });

    $hero.on('select.flickity', function () {
        checkArrows();
    });


    $('.hero__arrow--left').on('click', function () {
        $hero.flickity('previous');
    });

    $('.hero__arrow--right').on('click', function () {
        $hero.flickity('next');
    });

    function checkArrows() {
        var flickity = $hero.data('flickity');
        var cells = flickity.cells.length;
        var index = flickity.selectedIndex + 1;

        $('.hero__arrow').removeClass('hero__arrow--disabled');

        if (index === 1) {
            // First item
            $('.hero__arrow--left').addClass('hero__arrow--disabled');
        } else if (index == cells) {
            // Last item
            $('.hero__arrow--right').addClass('hero__arrow--disabled');
        }
    }

    checkArrows();
};

var footer = function () {
    var $footer = $('.footer');
    var $footerToggler = $('.footer__toggler');

    $footerToggler.on('click', function () {
        $footer.toggleClass('footer--open');
    });
};

var mobilenav = function () {
    $('.navicon').on('click', function () {
        $(this).toggleClass('navicon--active');
        $('.mobilenav').toggleClass('mobilenav--active');
    });
};

var gallery = function () {
    var $gallery = $('.gallery');

    var isotope = function () {
        $gallery.isotope({
            itemSelector: '.gallery__item',
            layoutMode: 'masonry'
        });

        setTimeout(function () {
            $gallery.isotope('layout');
        }, 2000);

        $(window).on('load', function () {
            $gallery.isotope('layout');
        });
    };

    var lightbox = function () {
        $('.js-open-gallery').on('click', function () {
            var items = [];
            $(this).parents('.gallery').find('.js-open-gallery').each( function (index) {
                items[index] = {
                    title: $(this).attr('data-title') ? $(this).attr('data-title') : '',
                    src: $(this).attr('data-lightbox-src'),
                    msrc: $(this).attr('src') ? $(this).attr('src') : $(this).find('img').attr('src'),
                    w: $(this).attr('data-lightbox-width'),
                    h: $(this).attr('data-lightbox-height')
                };
            });

            console.log(items, (parseInt($(this).attr('data-index'))));

            if (items.length >= 1) {
                var pswpElement = document.querySelectorAll('.pswp')[0];

                var options = {
                    index: $(this).attr('data-index') ? parseInt($(this).attr('data-index')) : null,
                    shareEl: false
                };

                var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            }
        });
    };

    isotope();
    lightbox();
};

$(document).ready(function () {

    if ($('.hero').length >= 1) {
        hero();
    }

    mobilenav();
    footer();
    gallery();
});