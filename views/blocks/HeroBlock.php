<?php
/**
 * View file for block: HeroBlock 
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4. 
 *
 * @param $this->extraValue('images');
 * @param $this->varValue('images');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>


<div class="hero">
    <div class="hero__items">
        <? foreach($this->extraValue('images', []) as $image): ?>
            <div class="hero__item">
                <div class="hero__image" style="background-image: url(<?= $image->source ?>)"></div>
            </div>
        <? endforeach; ?>
    </div>

    <div class="hero__arrow hero__arrow--left">
        <i class="fa fa-arrow-left"></i>
    </div>
    <div class="hero__arrow hero__arrow--right">
        <i class="fa fa-arrow-right"></i>
    </div>
</div>
