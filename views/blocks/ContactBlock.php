<?php
/**
 * View file for block: ContactBlock 
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4. 
 *
 * @param $this->varValue('city');
 * @param $this->varValue('days');
 * @param $this->varValue('days2');
 * @param $this->varValue('email');
 * @param $this->varValue('mapsEmbeddedLink');
 * @param $this->varValue('phone');
 * @param $this->varValue('street');
 * @param $this->varValue('time');
 * @param $this->varValue('time2');
 * @param $this->varValue('zip');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>

<div class="contact">
    <div class="contact__left">
        <h3>Adresse</h3>
        <p>
            <?= $this->varValue('phone', '') ?><br />
            <a href="mailto:<?= $this->varValue('email', '') ?>" target="_blank"><?= $this->varValue('email', '') ?></a>
        </p>
        <h3>Kontakt</h3>
        <p>
            <?= $this->varValue('street', '') ?><br />
            <?= $this->varValue('zip', '') ?> <?= $this->varValue('city', '') ?>
        </p>
        <h3>Öffnungszeiten</h3>
        <p>
            <strong><?= $this->varValue('days', '') ?></strong><br />
            <?= $this->varValue('time', '') ?>
            <br /><br />
            <strong><?= $this->varValue('days2', '') ?></strong><br />
            <?= $this->varValue('time2', '') ?>
        </p>
    </div>
    <div class="contact__right">
        <div class="contact__map-wrapper">
            <iframe class="contact__map" src="<?= $this->varValue('mapsEmbeddedLink', '') ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</div>
