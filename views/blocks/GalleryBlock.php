<?php
/**
 * View file for block: GalleryBlock 
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4. 
 *
 * @param $this->extraValue('images');
 * @param $this->varValue('images');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>

<div class="gallery">

    <? foreach($this->extraValue('images', []) as $index => $images): ?>
        <?
            /* @var $image \luya\admin\image\Item */
            /* @var $lightboxImage \luya\admin\image\Item */
            $lightboxImage = $images['lightboxImage'];
            $image = $images['image'];

            $caption = $images['additionalCaption'] ? $images['additionalCaption'] : ($image->caption ? $image->caption : false);
        ?>

        <div class="gallery__item<?= $image->caption ? ' gallery__item--has-info' : '' ?>">
            <div
                 class="gallery__item-inner js-open-gallery"
                 data-index="<?= $index ?>"
                 data-lightbox-src="<?= $lightboxImage->source ?>"
                 data-lightbox-width="<?= $lightboxImage->resolutionWidth ?>"
                 data-lightbox-height="<?= $lightboxImage->resolutionHeight ?>"
                 <? if($caption): ?>data-title="<?= $caption ?>"<? endif; ?>
            >
                <img class="gallery__image" src="<?= $image->source ?>" />
                <? if($caption): ?>
                    <div class="gallery__info">
                        <span><?= $caption ?></span>
                    </div>
                <? endif; ?>
            </div>
        </div>

    <? endforeach; ?>

</div>
