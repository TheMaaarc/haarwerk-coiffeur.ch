<?php

use app\assets\ResourcesAsset;
use luya\helpers\Url;
use luya\cms\widgets\LangSwitcher;

ResourcesAsset::register($this);

/* @var $this luya\web\View */
/* @var $content string */

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->composition->language; ?>" <?= Yii::$app->menu->current->isHome ? 'class="is-homepage"' : '' ?>>
    <head>
        <meta charset="UTF-8" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Favicons -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://haarwerk-coiffeur.ch/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://haarwerk-coiffeur.ch/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://haarwerk-coiffeur.ch/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://haarwerk-coiffeur.ch/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://haarwerk-coiffeur.ch/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://haarwerk-coiffeur.ch/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="https://haarwerk-coiffeur.ch/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="https://haarwerk-coiffeur.ch/favicon-16x16.png" sizes="16x16" />
        <meta name="application-name" content="Coiffeur Haarwerk Münchenstein"/>
        <meta name="msapplication-TileColor" content="#1F1F23" />
        <meta name="msapplication-TileImage" content="https://haarwerk-coiffeur.ch/mstile-144x144.png" />
        <!-- /Favicons -->

        <title><?= $this->title; ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

        <header class="header">
            <div class="container">
                <div class="header__navicon">
                    <div class="navicon">
                        <span class="navicon__line"></span>
                        <span class="navicon__line"></span>
                        <span class="navicon__line"></span>
                    </div>
                </div>
                <div class="header__mainnav">
                    <div class="mainnav">
                        <ul class="mainnav__list">
                            <? foreach(Yii::$app->menu->getLevelContainer(1) as $item): ?>
                                <li class="mainnav__item">
                                    <a class="mainnav__link" href="<?= $item->link ?>">
                                        <? if($item->isHome): ?>
                                            <i class="fa fa-home"></i>
                                        <? else: ?>
                                            <?= $item->title ?>
                                        <? endif; ?>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>

                <div class="header__logo logo">
                    <a class="logo__link" href="<?= Yii::$app->menu->home->link ?>">
                        <img class="logo__image" src="<?= $this->publicHtml ?>/images/logo.png" />
                    </a>
                </div>
            </div>
        </header>

        <div class="mobilenav">
            <ul class="mobilenav__list">
                <? foreach(Yii::$app->menu->getLevelContainer(1) as $item): ?>
                    <li class="mobilenav__item">
                        <a class="mobilenav__link" href="<?= $item->link ?>">
                            <?= $item->title ?>
                        </a>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>

        <?= $content ?>

        <footer class="footer">
            <div class="footer__toggler"></div>
            <div class="container">
                <div class="footer__section">
                    <p>
                        <strong>Adresse</strong><br />
                        Hauptstrasse 51<br />
                        4142 Münchenstein/BL
                    </p>
                    <p>
                        <strong>Öffnungszeiten</strong><br />
                        Dienstag - Freitag<br />
                        08:00 - 12:00, 13:30 - 18:30
                        <br /><br />
                        Samstag<br />
                        08:00 - 15:00
                    </p>
                </div>
                <div class="footer__section">
                    <p>
                        <strong>Kontakt</strong><br />
                        061 411 15 51<br />
                        <a href="mailto:info@coiffeur-haarwerk.ch" target="_blank">info@haarwerk-coiffeur.ch</a>
                    </p>
                </div>
                <div class="footer__section footer__section--socials">
                    <a class="footer__facebook" href="https://www.facebook.com/HaarwerkMuenchenstein/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a class="footer__instagram" href="https://www.instagram.com/haarwerk_muenchenstein/" target="_blank"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </footer>

        <!-- Photoswipe dom element -->
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"> <div class="pswp__bg"></div> <div class="pswp__scroll-wrap"> <div class="pswp__container"> <div class="pswp__item"></div> <div class="pswp__item"></div> <div class="pswp__item"></div> </div> <div class="pswp__ui pswp__ui--hidden"> <div class="pswp__top-bar"> <div class="pswp__counter"></div> <button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title="Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button> <div class="pswp__preloader"> <div class="pswp__preloader__icn"> <div class="pswp__preloader__cut"> <div class="pswp__preloader__donut"></div> </div> </div> </div> </div> <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"> <div class="pswp__share-tooltip"></div> </div> <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"> </button> <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"> </button> <div class="pswp__caption"> <div class="pswp__caption__center"></div> </div> </div> </div> </div>

    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
