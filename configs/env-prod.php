<?php

/**
 * This config should be used on production enviroment.
 * The production enviroment is the last "step" and is visible to all.
 *
 * The production configuration is mostly a copei of the local or prep configuration with additional database
 * component, for example:
 *
 * ```php
 * db' => [
 *     'class' => 'yii\db\Connection',
 *     'dsn' => 'mysql:host=localhost;dbname=DB_NAME',
 *     'username' => 'DB_USER',
 *     'password' => 'DB_PASS',
 *     'charset' => 'utf8',
 *     // 'enableSchemaCache' => true,
 *     // 'schemaCacheDuration' => 43200,
 * ]
 * ```
 *
 * In production environemtn `YII_ENV` is by default set to `prod` and `YII_DEBUG` is false by default.
 */

return [

    /*
     * For best interoperability it is recommended that you use only alphanumeric characters when specifying an application ID
     */
    'id' => 'myproject',

    /*
     * The name of your site, will be display on the login screen
     */
    'siteTitle' => 'My Project',

    /*
     * Let the application know which module should be executed by default (if no url is set). This module must be included
     * in the modules section. In the most cases you are using the cms as default handler for your website. But the concept
     * of LUYA is also that you can use the Website without the CMS module!
     */
    'defaultRoute' => 'cms',

    /*
     * Define the basePath of the project (Yii Configration Setup)
     */
    'basePath' => dirname(__DIR__),

    'modules' => [

        /*
         * If you have other admin modules (like cmsadmin) then you going to need the admin. The Admin module provides
         * a lot of functionality, like storage, user, permission, crud, etc. But the basic concept of LUYA is also that you can use LUYA without the
         * admin module.
         *
         * @secureLogin: (boolean) This will activate a two-way authentification method where u get a token sent by mail, for this feature
         * you have to make sure the mail component is configured correctly. You can test this with console command `./vendor/bin/luya health/mailer`.
         */
        'admin' => [
            'class' => 'luya\admin\Module',
            'secureLogin' => false, // when enabling secure login, the mail component must be proper configured otherwise the auth token mail will not send.
            'interfaceLanguage' => 'en',
        ],

        /*
         * Frontend module for the `cms` module.
         */
        'cms' => [
            'class' => 'luya\cms\frontend\Module',
            'contentCompression' => true, // compressing the cms output (removing white spaces and newlines)
        ],

        /*
         * Admin module for the `cms` module.
         */
        'cmsadmin' => [
            'class' => 'luya\cms\admin\Module',
            'hiddenBlocks' => [],
            'blockVariations' => [],
        ],
    ],
    'components' => [

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=marcstam_coiffeurhaarwerk',
            'username' => 'marcstam_coiffeu',
            'password' => 'HU#2yA!6HAu&3B',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 43200,
        ],

        /*
         * Add your smtp connection to the mail component to send mails (which is required for secure login), you can test your
         * mail component with the luya console command ./vendor/bin/luya health/mailer.
         */
        'mail' => [
            'host' => null,
            'username' => null,
            'password' => null,
            'from' => null,
            'fromName' => null,
        ],

        /*
         * ATTENTION:
         * To help us improve our Software you can enable (true) this property to send all Exceptions directly to the luya developer team.
         * The follwing informations will be transfered:
         * - $_GET, $_POST, $_SERVER and $_SESSION data
         * - Exception Object (inlcuding stack trace, line, linenr, message, file)
         *
         * You can also create your own errorapi (https://github.com/luyadev/luya-module-errorapi) module to get notification
         * about the errors from your projects.
         */
        'errorHandler' => [
            'transferException' => false,
        ],

        /*
         * The composition component handles your languages and they way your urls will look like. The composition componentn will
         * automatically add the language prefix you have defined in `default` to your url (the language part in the url "example.com/EN/homepage").
         *
         * hidden: (boolean) If this website is not multilingual you can hidde the composition, other whise you have to enable this.
         * default: (array) Contains the default setup for the current language, this must match your language system configuration.
         */
        'composition' => [
            'hidden' => true, // you will not have languages in your url (most case for pages which are not multi lingual)
            'default' => ['langShortCode' => 'de'], // the default language for the composition should match your default language shortCode in the langauge table.
        ],

        /*
         * When you are enabling the cache, luya will cache cms blocks and speed up the system in different ways. In the prep config
         * we use the DummyCache to "fake" the caching behavior, but actually nothing gets cached, when your in production you should
         * use caching which matches your hosting environment. In most cases yii\caching\FileCache will result in fast website.
         *
         * http://www.yiiframework.com/doc-2.0/guide-caching-data.html#cache-apis
         */
        'cache' => [
            'class' => 'yii\caching\FileCache', // use: yii\caching\FileCache
        ],
    ],
];
