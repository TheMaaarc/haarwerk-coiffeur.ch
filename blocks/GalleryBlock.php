<?php

namespace app\blocks;

use app\filters\GalleryFilter;
use app\filters\LightboxFilter;
use luya\admin\filters\SmallThumbnail;
use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Gallery Block.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4. 
 */
class GalleryBlock extends PhpBlock
{
    /**
     * @var bool Choose whether a block can be cached trough the caching component. Be carefull with caching container blocks.
     */
    public $cacheEnabled = true;
    
    /**
     * @var int The cache lifetime for this block in seconds (3600 = 1 hour), only affects when cacheEnabled is true
     */
    public $cacheExpiration = 3600;

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Gallery Block';
    }
    
    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }
 
    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'images', 'label' => 'Bilder', 'type' => self::TYPE_IMAGEUPLOAD_ARRAY, 'options' => ['no_filter' => false]],
            ],
        ];
    }
    
    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        $images = [];

        foreach($this->getVarValue('images', []) as $image) {
            $images[] = [
                'image' => BlockHelper::imageUpload($image['imageId'], GalleryFilter::identifier(), true),
                'lightboxImage' => BlockHelper::imageUpload($image['imageId'], LightboxFilter::identifier(), true),
                'additionalCaption' => $image['caption']
            ];
        }

        return [
            'images' => $images,
            'adminImages' => BlockHelper::imageArrayUpload($this->getVarValue('images'), SmallThumbnail::identifier(), true),
        ];
    }

    /**
     * {@inheritDoc} 
     *
     * @param {{extras.images}}
     * @param {{vars.images}}
    */
    public function admin()
    {
        return '<div class="clearfix" style="display: block; margin: -15px 0 0 -15px;">
                    {% for image in extras.adminImages %}
                        <img src="{{ image.source }}" style="display: inline-block; float: left; margin: 15px 0 0 15px;" />
                    {% endfor %}
                </div>';
    }
}