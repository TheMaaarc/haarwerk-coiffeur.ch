<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Contact Block.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4. 
 */
class ContactBlock extends PhpBlock
{
    /**
     * @var bool Choose whether a block can be cached trough the caching component. Be carefull with caching container blocks.
     */
    public $cacheEnabled = true;
    
    /**
     * @var int The cache lifetime for this block in seconds (3600 = 1 hour), only affects when cacheEnabled is true
     */
    public $cacheExpiration = 3600;

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Kontakt';
    }
    
    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }
 
    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'phone', 'label' => 'Telefon', 'type' => self::TYPE_TEXT],
                 ['var' => 'email', 'label' => 'E-Mail', 'type' => self::TYPE_TEXT],
                 ['var' => 'street', 'label' => 'Strasse', 'type' => self::TYPE_TEXT],
                 ['var' => 'zip', 'label' => 'PLZ', 'type' => self::TYPE_TEXT],
                 ['var' => 'city', 'label' => 'Ort', 'type' => self::TYPE_TEXT],
                 ['var' => 'days', 'label' => 'Tage', 'type' => self::TYPE_TEXT],
                 ['var' => 'days2', 'label' => 'Tage 2', 'type' => self::TYPE_TEXT],
                 ['var' => 'time', 'label' => 'Zeit (Tage)', 'type' => self::TYPE_TEXT],
                 ['var' => 'time2', 'label' => 'Zeit (Tage 2)', 'type' => self::TYPE_TEXT],
                 ['var' => 'mapsEmbeddedLink', 'label' => 'Einbettungslink Karte', 'type' => self::TYPE_TEXT],
            ],
        ];
    }
    
    /**
     * {@inheritDoc} 
     *
     * @param {{vars.city}}
     * @param {{vars.days2}}
     * @param {{vars.days}}
     * @param {{vars.email}}
     * @param {{vars.mapsEmbeddedLink}}
     * @param {{vars.phone}}
     * @param {{vars.street}}
     * @param {{vars.time2}}
     * @param {{vars.time}}
     * @param {{vars.zip}}
    */
    public function admin()
    {
        return '<p>Contact Block Admin View</p>';
    }
}