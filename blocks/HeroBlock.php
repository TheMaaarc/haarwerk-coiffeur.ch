<?php

namespace app\blocks;

use app\filters\HeroFilter;
use app\filters\HeroImageFilter;
use luya\admin\filters\SmallThumbnail;
use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Hero Block.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4. 
 */
class HeroBlock extends PhpBlock
{
    /**
     * @var bool Choose whether a block can be cached trough the caching component. Be carefull with caching container blocks.
     */
    public $cacheEnabled = true;
    
    /**
     * @var int The cache lifetime for this block in seconds (3600 = 1 hour), only affects when cacheEnabled is true
     */
    public $cacheExpiration = 3600;

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Hero';
    }
    
    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }
 
    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'images', 'label' => 'Bilder', 'type' => self::TYPE_IMAGEUPLOAD_ARRAY, 'options' => ['no_filter' => false]],
            ],
        ];
    }
    
    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        return [
            'images' => BlockHelper::imageArrayUpload($this->getVarValue('images'), HeroImageFilter::identifier(), true),
            'adminImages' => BlockHelper::imageArrayUpload($this->getVarValue('images'), SmallThumbnail::identifier(), true),
        ];
    }

    /**
     * {@inheritDoc} 
     *
     * @param {{extras.images}}
     * @param {{vars.images}}
    */
    public function admin()
    {
        return '<div class="clearfix" style="display: block; margin: -15px 0 0 -15px;">
                    {% for image in extras.adminImages %}
                        <img src="{{ image.source }}" style="display: inline-block; float: left; margin: 15px 0 0 15px;" />
                    {% endfor %}
                </div>';
    }
}