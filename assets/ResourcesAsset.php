<?php

namespace app\assets;

class ResourcesAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/resources';
    
    public $js = [
        YII_ENV . '/js/main.min.js'
    ];
    
    public $css = [
        YII_ENV . '/css/main.css'
    ];
    
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
